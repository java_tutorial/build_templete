# CI/CD Template for Build Job
---


## How to Use:
- Make sure you have the ``src`` folder at the root of your project.
- add new file as `.gitlab-ci.yml` file and add 
```markdown
  include:
  remote: 'https://gitlab.com/awesome-project/raw/master/.before-script-template.yml'
  ```
  
## How to configure GitLab with IntelliJ:
---
Open ``IntelliJ`` goto ``Settings`` 🔎 for ``servers`` you can find it under ``Tools`` ➡ ``Tasks`` ➡  `` Servers ``  

![](screenshots/settings.png)

- Click ➕ on the right side  of the  window  or  ``Alt+ Insert ``
- Find 
 
 ![](screenshots/gilab.png)
- Click on GitLab Icon to add GitLab you can see the following screen.
![](screenshots/configure.png)
- ``Server URL``: gitlab.com
- ``Token``: Enter your GitLab token.
- ``Project``: If you have already created a project in GitLab you can see your project name under the ``dropdown`` otherwise leave it as is ``Set server URL and token first``.
- Click on ``Apply`` and ``OK``.
- 😊 GitLab is Configured ✅ 
    
